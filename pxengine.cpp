
#ifdef MINGW
	#include "pxengine_mingw.h"
#endif
#include "pxengine.h"
using namespace px_render;

double __random__() { return ((double)std::rand() / (RAND_MAX)); };
static std::map<std::string, std::string> VShaders = {};
	static std::map<std::string, std::string> FShaders = {};
	static std::map<char, std::vector<Vec3>> SharedPos = {};
	static std::map<char, std::vector<Vec3>> SharedRot = {};
	static std::map<char, std::vector<Vec3>> SharedScl = {};
	uint16_t get_shared_id(std::map<char, std::vector<Vec3>> table, char group, float x, float y, float z){
		auto found=false;
		uint16_t idx=0;
		if(table.count(group)==0){
			table[group]=std::vector<Vec3>();
		}
		for(Vec3 &vec: table[group]){
			if(vec.v.x==x && vec.v.y==y && vec.v.z==z){
				found=true;
				break;
			}
			idx++;
		}
		if (found==true){
			std::cout << "got shared vec" << idx << std::endl;
		} else {
			idx=(uint16_t)table[group].size();
			table[group].push_back({x,y,z});
		}
		return idx;
	};
struct object3D {
		char group:5;
		char frame:4;
		uint16_t _rot:9;
		uint16_t _scl:12;
		uint16_t _pos:14;
		float x(){return SharedPos[group][_pos].v.x;}
		float y(){return SharedPos[group][_pos].v.y;}
		float z(){return SharedPos[group][_pos].v.z;}
		void x(float v){SharedPos[group][_pos].v.x=v;}
		void y(float v){SharedPos[group][_pos].v.y=v;}
		void z(float v){SharedPos[group][_pos].v.z=v;}
		void pos(float _x,float _y, float _z){
			x(_x);
			y(_y);
			z(_z);
		}
		object3D(std::string grp, float x,float y, float z, float rx, float ry, float rz, float sx, float sy, float sz){
			std::cout<<"x:"<<x <<",y:"<<y <<",z:"<<z <<std::endl;
			group=grp[0];
			_pos=get_shared_id(SharedPos, group, x,y,z);
			_rot=get_shared_id(SharedRot, group, rx,ry,rz);
			_scl=get_shared_id(SharedScl, group, sx,sy,sz);
		}
	};
float rand(float a) {

	
	return ((__random__() - 0.5) * a);
}
static unsigned int BATCH_SIZE=1024;
	struct PxEngineAPI {
		int screen_width;
		int screen_height;
		RenderContext ctx;
		Mat4 proj;
		Mat4 view;
		std::map<std::string, std::vector<object3D>> objects;
	};
	static PxEngineAPI PxEngine = {1024,768};
	struct mesh {
		std::string name;
		std::vector<float> verts;
		std::vector<uint16_t> indices;
		std::vector<Vec3> pos;
		std::vector<Vec3> rot;
		std::vector<Vec3> scl;
		std::vector<object3D> instances;
		Buffer vbuff;
		Buffer ibuff;
		Buffer posbuff;
		Buffer rotbuff;
		Buffer sclbuff;
		Pipeline material;
		float spin=0;
		mesh(std::vector<float> vs, std::vector<uint16_t> ts, std::string matname) {
			std::cout<<"making mesh"<<std::endl;
			std::cout<<VShaders[matname]<<std::endl;
			verts=vs;
			indices=ts;
			auto info = Pipeline::Info();
			info.depth_write = false;
			info.blend.enabled = true;
			info.blend.dst_alpha = BlendFactor::BlendAlpha;
			info.primitive = Primitive::Lines;
			info.depth_write = false;
			info.shader.vertex   = VShaders[matname].c_str();
			info.shader.fragment = FShaders[matname].c_str();
			info.attribs[0] = {"position", VertexFormat::Float3};
			info.attribs[1] = {"instance_position", VertexFormat::Float3, 1, VertexStep::PerInstance};
			info.attribs[2] = {"instance_rotation", VertexFormat::Float3, 2, VertexStep::PerInstance};
			info.attribs[3] = {"instance_scale",    VertexFormat::Float3, 3, VertexStep::PerInstance};
			std::cout<<"making material..."<<std::endl;
			material = PxEngine.ctx.createPipeline(info);
			vbuff=PxEngine.ctx.createBuffer({BufferType::Vertex, sizeof(float)*verts.size(), Usage::Static});
			ibuff=PxEngine.ctx.createBuffer({BufferType::Vertex, sizeof(uint16_t)*indices.size(), Usage::Static});
			DisplayList dl;
			dl.fillBufferCommand()
			  .set_buffer(vbuff)
			  .set_data(verts.data())
			  .set_size(sizeof(float)*verts.size());
			dl.fillBufferCommand()
			  .set_buffer(ibuff)
			  .set_data(indices.data())
			  .set_size(sizeof(uint16_t)*indices.size());
			posbuff=PxEngine.ctx.createBuffer({BufferType::Vertex, sizeof(Vec3)*BATCH_SIZE, Usage::Static});
			rotbuff=PxEngine.ctx.createBuffer({BufferType::Vertex, sizeof(Vec3)*BATCH_SIZE, Usage::Static});
			sclbuff=PxEngine.ctx.createBuffer({BufferType::Vertex, sizeof(Vec3)*BATCH_SIZE, Usage::Static});
			for(int i=0;i<BATCH_SIZE;i++){
				pos.push_back({0,0,0});
				rot.push_back({0,0,0});
				scl.push_back({1,1,1});
			}
			std::cout<<"buffers ready"<<std::endl;
			PxEngine.ctx.submitDisplayListAndSwap(std::move(dl));
			while(true){
				auto result = PxEngine.ctx.executeOnGPU();
				if(result != RenderContext::Result::OK) break;
			}
		}
		uint16_t add_instance(float x, float y, float z){
			auto ob= object3D(name, x,y,z, 0,0,0, 1,1,1);
			instances.push_back(ob);
			return (uint16_t)instances.size()-1;
		}
		void set_pos(uint16_t id, float x, float y, float z){
			instances[id].pos(x,y,z);
		}
		void render_batch(int num){
			auto model = Mat4();
			gb_mat4_rotate((gbMat4*)model.f, {0,0,1}, spin);
			DisplayList dl;
			dl.fillBufferCommand()
			  .set_buffer(posbuff)
			  .set_data(pos.data())
			  .set_size(sizeof(Vec3)*pos.size());
			dl.fillBufferCommand()
			  .set_buffer(rotbuff)
			  .set_data(rot.data())
			  .set_size(sizeof(Vec3)*rot.size());
			dl.fillBufferCommand()
			  .set_buffer(sclbuff)
			  .set_data(scl.data())
			  .set_size(sizeof(Vec3)*scl.size());
			dl.setupPipelineCommand()
				.set_pipeline(material)
				.set_buffer(0,vbuff)
				.set_buffer(1, posbuff)
				.set_buffer(2, rotbuff)
				.set_buffer(3, sclbuff)
				.set_model_matrix(model)
			;
			dl.renderCommand()
				.set_index_buffer(ibuff)
				.set_count(indices.size())
				.set_type(IndexFormat::UInt16)
				.set_instances( num )
			;
			PxEngine.ctx.submitDisplayListAndSwap(std::move(dl));
			while(true){
				auto result = PxEngine.ctx.executeOnGPU();
				if(result != RenderContext::Result::OK) break;
			}
		}
		void render(){
			std::cout<<"rendering..."<<std::endl;
			spin+=0.01;
			int idx=0;
			for(auto &ob: instances){
				pos[idx].v.x=ob.x();
				pos[idx].v.y=ob.y();
				pos[idx].v.z=ob.z();
				idx++;
				if(idx == BATCH_SIZE){
					render_batch(BATCH_SIZE);
					idx=0;
				}
			}
			if(idx){
				render_batch(idx);
			}
		}
	};
	static std::vector<mesh> Meshes;
auto add_object(std::string name, float x, float y, float z) {

	
	auto ob= object3D(name, x,y,z, 0,0,0, 1,1,1);
		if(PxEngine.objects.count(name)==0){
			PxEngine.objects[name]=std::vector<object3D>();
		}
		PxEngine.objects[name].push_back(ob);
	std::cout << std::string("sizeof ob3d:")<<sizeof(ob) << std::endl;
	return ob;
}
void on_cleanup() {

	
		PxEngine.ctx.finish();
}
void font_parsed(void* args, void* fdata, int error) {

	
	std::cout << std::string("font parsed") << std::endl;
	if (error) {
		return;
	}
	auto mult = 0.003;  /* auto-fallback <_ast.Constant object at 0x7fea1a765c40> */
	TTFFontParser::FontData* font_data = (TTFFontParser::FontData*)fdata;
		printf("Font: %s %s parsed\n", font_data->font_names.begin()->font_family.c_str(), font_data->font_names.begin()->font_style.c_str());
		printf("Number of glyphs: %d\n", uint32_t(font_data->glyphs.size()));
		int max=0;
		for (const auto& glyph_iterator : font_data->glyphs) {
			max++;
			if(max>5)break;
			uint32_t num_curves = 0, num_lines = 0;
			std::cout << "glyph id=" << glyph_iterator.first <<std::endl;
			std::vector<float> verts = std::vector<float>();
			std::vector<uint16_t> indices = std::vector<uint16_t>();
			int index=0;
			for (const auto& path_list : glyph_iterator.second.path_list) {
				for (const auto& geo : path_list.geometry) {
					if (geo.is_curve){
						num_curves++;
					} else {
						num_lines++;
						//std::cout << " x1=" << (geo.p0.x*mult) << " y1=" << (geo.p0.y*mult) <<std::endl;
					}
					verts.push_back(geo.p0.x*mult);
					verts.push_back(geo.p0.y*mult);
					verts.push_back(0);
					verts.push_back(geo.p1.x*mult);
					verts.push_back(geo.p1.y*mult);
					verts.push_back(0);
					indices.push_back(index);
					indices.push_back(index+1);
					index+=2;
				}
			}
			std::cout << " num curves=" << num_curves <<std::endl;
			std::cout << " num lines=" << num_lines <<std::endl;
			if (verts.size()>2){
				auto m = mesh(verts, indices, "line2d");
				Meshes.push_back(m);			
			}
		};
	std::cout << std::string("font parsed OK") << std::endl;
		std::cout << std::string("num Meshes")<<Meshes.size() << std::endl;
}
void init_fonts() {

	
	std::string fontname = std::string("pasta");
	for (int i=0; i<32; i++) {
		auto fname = ((fontname + str(i)) + std::string(".ttf"));  /* auto-fallback <_ast.BinOp object at 0x7fea1a6346d0> */
		if (__file_exist__(fname)==true) {
			std::cout << std::string("loading font:")<<fname << std::endl;
			uint8_t condition_variable = 0;
				TTFFontParser::FontData font_data;
				int8_t error = TTFFontParser::parse_file(fname.c_str(), &font_data, &font_parsed, &condition_variable);
		}
	}
}
void init_shaders() {

	
	std::cout << std::string("init shaders...") << std::endl;
	auto names = std::make_shared<std::vector<std::string>>(std::vector<std::string>{std::string("line"),std::string("fill")});  /* auto-fallback <_ast.List object at 0x7fea1a634f40> */
	for (auto &k: *names) { /*loop over unknown type*/
		auto v = (k + std::string(".vshader.glsl"));  /* auto-fallback <_ast.BinOp object at 0x7fea1a7150d0> */
		std::cout << std::string("checking for file:")<<v << std::endl;
		if (__file_exist__(v)==true) {
			std::cout << std::string("loading vertex shader:")<<v << std::endl;
			auto vdata = __readfile__(v, std::string("rb"));  /* auto-fallback <_ast.Call object at 0x7fea1a715520> */
			std::cout << vdata << std::endl;
			VShaders[k]=vdata;
		}
		auto f = (k + std::string(".fshader.glsl"));  /* auto-fallback <_ast.BinOp object at 0x7fea1a715880> */
		std::cout << std::string("checking for file:")<<f << std::endl;
		if (__file_exist__(f)==true) {
			std::cout << std::string("loading fragment shader:")<<f << std::endl;
			auto fdata = __readfile__(f, std::string("rb"));  /* auto-fallback <_ast.Call object at 0x7fea1a715cd0> */
			FShaders[k]=fdata;
		}
	}
}
void add_font() {

	
	std::string ttf = std::string("");
	std::getline(std::cin, ttf);
		uint8_t condition_variable = 0;
		TTFFontParser::FontData font_data;
		int8_t error = TTFFontParser::parse_file(ttf.c_str(), &font_data, &font_parsed, &condition_variable);
}
void add_vertex_shader() {

	
	std::cout << std::string("add vshader...") << std::endl;
	std::string vs = std::string("");
	std::getline(std::cin, vs);
	std::cout << vs << std::endl;
	auto data = __readfile__(vs, std::string("rb"));  /* auto-fallback <_ast.Call object at 0x7fea1a726760> */
	std::cout << data << std::endl;
	VShaders[vs]=data;
}
void add_frag_shader() {

	
	std::cout << std::string("add fshader...") << std::endl;
	std::string fs = std::string("");
	std::getline(std::cin, fs);
	std::cout << fs << std::endl;
	auto data = __readfile__(fs, std::string("rb"));  /* auto-fallback <_ast.Call object at 0x7fea1a726fd0> */
	std::cout << data << std::endl;
	FShaders[fs] = data;
}
void on_init() {

	
	std::cout << std::string("px init...") << std::endl;
	#ifdef PX_RENDER_BACKEND_GL
		gladLoadGL();
	#endif
	std::cout << std::string("setup engine...") << std::endl;
		PxEngine.ctx.init();
	std::cout << std::string("opengl context init OK") << std::endl;
	PxEngine.objects = std::map<std::string, std::vector<object3D>>();
		gb_mat4_perspective(( (gbMat4*)&PxEngine.proj ), gb_to_radians(45.0), (PxEngine.screen_width / __float__(PxEngine.screen_height)), 0.05, 9000.0);
	gb_mat4_look_at(( (gbMat4*)&PxEngine.view ), {0.0,1.0,- (10.0)}, {0.0,0.0,0.0}, {0.0,0.0,1.0});
	init_shaders();
	init_fonts();
}
std::vector<float> getvec3bytes() {

	
	auto v=std::vector<float>();
		char c=' ';
	for (int i=0; i<3; i++) {
		std::cin >> c;
			float n = static_cast<float>(c)/255.0;
			v.push_back(n);
	}
	return v;
}
std::vector<float> getvec(int num) {

	
	auto v=std::vector<float>();
	for (int i=0; i<(num * 2); i++) {
		char c = ' ';
			int16_t n = 0;
			std::cin >> c;
			n |= (unsigned char)c;
			std::cin >> c;
			n |= c << 8;
			std::cout<<"unpacked:" << n << std::endl;
			float a = static_cast<float>(n)*0.01;
			v.push_back(a);
	}
	return v;
}
std::vector<float> getvec3() {

	
	return getvec(3);
}
std::vector<float> getvec4() {

	
	return getvec(4);
}
void add_object() {

	
	std::string name = std::string("none");
	std::getline(std::cin, name);
	std::cout << std::string("adding:")<<name << std::endl;
	auto v = getvec3();			/* newvariable*/
		add_object(name, v[0], v[1], v[2]);
}
std::vector<float> BG=std::vector<float>{0.5,0.5,0.5, 1.0};
std::string CMD = std::string("");
void on_frame() {

	
	std::string cmd = std::string("");
	if ( CMD == std::string("bg") ) {
		BG = getvec4();
		CMD = std::string("");
	} else {
		if ( CMD == std::string("object") ) {
			add_object();
			CMD = std::string("");
		} else {
			if ( CMD == std::string("font") ) {
				add_font();
				CMD = std::string("");
			} else {
				if ( CMD == std::string("vshader") ) {
					add_vertex_shader();
					CMD = std::string("");
				} else {
					if ( CMD == std::string("fshader") ) {
						add_frag_shader();
						CMD = std::string("");
					} else {
						std::getline(std::cin, cmd);
					}
				}
			}
		}
	}
	if ( cmd == std::string("bg") ) {
		std::cout << std::string("set background color") << std::endl;
		CMD = cmd;
	} else {
		if ( cmd == std::string("object") ) {
			std::cout << std::string("set object") << std::endl;
			CMD = cmd;
		} else {
			if ( cmd == std::string("font") ) {
				CMD = cmd;
			} else {
				if ( cmd == std::string("vshader") ) {
					CMD = cmd;
				} else {
					if ( cmd == std::string("fshader") ) {
						CMD = cmd;
					} else {
						if ( cmd == std::string("exit") ) {
							throw "exit";
						}
					}
				}
			}
		}
	}
	auto dl = DisplayList();			/* newvariable*/
	dl.setupViewCommand()
			.set_viewport({0,0,PxEngine.screen_width,PxEngine.screen_height})
			.set_projection_matrix(PxEngine.proj)
			.set_view_matrix(PxEngine.view)
		;
		dl.clearCommand()
			.set_color({BG[0],BG[1],BG[2], BG[3]})
			.set_clear_color(true)
			.set_clear_depth(true)
		;
		for (auto &pair: PxEngine.objects) { /*stack-loop-over unknown type*/
		auto name = pair.first;  /* auto-fallback <_ast.Attribute object at 0x7fea1a681250> */
		auto obs = pair.second;  /* auto-fallback <_ast.Attribute object at 0x7fea1a681310> */
		std::cout << std::string("name:")<<name << std::endl;
		for (auto &ob: obs) { /*stack-loop-over unknown type*/
			std::cout << std::string("x:")<<ob.x() << std::endl;
			std::cout << std::string("y:")<<ob.y() << std::endl;
			std::cout << std::string("z:")<<ob.z() << std::endl;
		}
	}
		PxEngine.ctx.submitDisplayListAndSwap(std::move(dl));
	while (true) {
		auto result = PxEngine.ctx.executeOnGPU();  /* auto-fallback <_ast.Call object at 0x7fea1a681d30> */
		if ( result != RenderContext::Result::OK ) {
			break;
		}
	}
	for (auto &m: Meshes) { /*stack-loop-over unknown type*/
		std::cout << std::string("rendering mesh...") << std::endl;
		m.render();
	}
}
void on_input(const sapp_event* ev) {

	
	if ( ev->type != SAPP_EVENTTYPE_KEY_DOWN ) {
		if ( ev->type != SAPP_EVENTTYPE_KEY_UP ) {
			return;
		}
	}
	if ( ev->key_code == SAPP_KEYCODE_UP ) {
		std::cout << std::string("up") << std::endl;
	} else {
		if ( ev->key_code == SAPP_KEYCODE_DOWN ) {
			std::cout << std::string("down") << std::endl;
		} else {
			if ( ev->key_code == SAPP_KEYCODE_LEFT ) {
				std::cout << std::string("down") << std::endl;
			} else {
				if ( ev->key_code == SAPP_KEYCODE_RIGHT ) {
					std::cout << std::string("right") << std::endl;
				} else {
					std::cout << ev->key_code << std::endl;
				}
			}
		}
	}
}
/*main-entry-point*/
sapp_desc sokol_main(int argc, char** argv) {

	
	std::cout << std::string("sokol_main...") << std::endl;
	auto d = sapp_desc();			/* newvariable*/
		d.window_title = "RapterRender";
	d.init_cb = on_init;
	d.frame_cb = on_frame;
	d.cleanup_cb = on_cleanup;
	d.width = PxEngine.screen_width;
	d.height = PxEngine.screen_height;
	d.html5_canvas_resize = true;
	d.sample_count = 4;
	d.event_cb = on_input;
	return d;
}