#version 330
uniform mat4 u_modelViewProjection;
in vec3 vertex_position;
in vec3 instance_position;
in vec3 instance_rotation;
in vec3 instance_scale;
out vec4 v_color;
out vec3 viewPosition;
float PHI = 1.61803398874989484820459; 
float gold_noise(in vec2 xy, in float seed){
	return fract(tan(distance(xy*PHI, xy)*seed)*xy.x);
}

// Return rotation matrix
mat3 rotationMatrixXYZ(vec3 r) {
	float cx = cos(r.x);
	float sx = sin(r.x);
	float cy = cos(r.y);
	float sy = sin(r.y);
	float cz = cos(r.z);
	float sz = sin(r.z);
	return mat3(cy * cz, 	cx * sz + sx * sy * cz, 	sx * sz - cx * sy * cz,
			-cy * sz,	cx * cz - sx * sy * sz,		sx * cz + cx * sy * sz,
			sy,			-sx * cy,					cx * cy);
}

mat4 xFormXYZ(vec3 t, vec3 sc, vec3 r) {
	mat3 scale = mat3( sc.x, 0.0, 0.0,
	0.0, sc.y, 0.0,
	0.0, 0.0, sc.z);
	mat3 xfm = rotationMatrixXYZ(r)  * scale;
	return mat4(
		xfm[0], 0.0,
		xfm[1], 0.0,
		xfm[2], 0.0,
		t, 1.0
	);
}

void main() {
	v_color = vec4(0.0,0.0,0.0,0.01);
	mat4 xfm = xFormXYZ(instance_position, instance_scale, instance_rotation);
	vec4 worldspace = u_modelViewProjection * xfm * vec4(vertex_position,1.0);
	gl_Position = worldspace;
	viewPosition = worldspace.xyz;
}
