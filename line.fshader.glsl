#version 330
in vec4 v_color;
in vec3 viewPosition;
out vec4 color_out;

void main() {
	color_out = v_color;
}