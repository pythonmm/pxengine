#!/usr/bin/env python3
import os, sys, subprocess, struct, time
from random import *

shorthalf=2**15
smin= -0x7fff
smax= 0x7fff

if '--debug' in sys.argv:
	proc=subprocess.Popen(['./pxengine'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
else:
	proc=subprocess.Popen(['./pxengine'], stdin=subprocess.PIPE)

time.sleep(1)

def cmd(name, data=None):
	if type(name) is list:
		name='\n'.join(name)
	proc.stdin.write(name.encode('ascii')+b'\n')
	proc.stdin.flush()
	if data is not None:
		dat = b''
		for v in data:
			a=int(v*100)
			if a < smin:
				a=smin
			elif a > smax:
				a=smax
			print('packing: %s=%s' %(v,a))
			dat += struct.pack('h', a )
		proc.stdin.write(dat)
		proc.stdin.flush()


for i in range(30):
	time.sleep(0.2)
	cmd('bg', [int(uniform(-1,255)) for i in range(4)])
		

proc.stdin.write(b'exit\n')
proc.stdin.flush()


proc.kill()
